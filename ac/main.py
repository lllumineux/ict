from common import read_symbols, decode_binary


def decode_ac(num: float, length: int) -> str:
    probabilities = read_symbols('tests/1.txt', with_probabilities=True)
    symbols = list(probabilities)
    segments = [0]
    for p in probabilities.values():
        segments.append(segments[-1] + p)
    res = ''
    for _ in range(length):
        for i in range(len(symbols)):
            sym = symbols[i]
            left, right = segments[i], segments[i + 1]
            if left <= num < right:
                res += sym
                num = (num - left) / (right - left)
                break
    return res


def main():
    number = float(decode_binary(input('Enter number to decode (in binary): ')))
    text_length = int(decode_binary(input('Enter encoded text length (in binary): ')))
    decoded_text = decode_ac(number, text_length)
    print(f'Your decoded text: {decoded_text}')


if __name__ == '__main__':
    main()
