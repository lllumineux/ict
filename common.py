def encode_binary(text: str) -> str:
    return ' '.join(format(ord(x), 'b') for x in text)


def decode_binary(binary_text: str) -> str:
    return ''.join(chr(int(x, 2)) for x in binary_text.split())


def read_symbols(path, with_probabilities=False) -> list or dict:
    with open(path) as f:
        symbols = f.readline().split()
        if not with_probabilities:
            return symbols
        probabilities = [float(x.replace(',', '.')) for x in f.readline().split()]
        return {s: p for s, p in zip(symbols, probabilities)}
