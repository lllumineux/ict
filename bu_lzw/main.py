from common import encode_binary, read_symbols


def encode_bu(text: str) -> tuple[str, int]:
    n = len(text)
    lines = [text[i:n] + text[0:i] for i in range(n)]
    lines.sort()
    num = lines.index(text)
    return ''.join([line[-1] for line in lines]), num


def compress_lzw(text: str) -> str:
    lines = read_symbols('tests/1.txt')
    encoded_numbers = []
    current_line = ''
    for sym in text:
        new_line = current_line + sym
        if new_line in lines:
            current_line = new_line
        else:
            encoded_numbers.append(lines.index(current_line))
            lines.append(new_line)
            current_line = sym
    encoded_numbers.append(lines.index(current_line))
    return '^'.join(str(num) for num in encoded_numbers)


def main():
    text = input('Enter text to encode: ')
    encoded_text, number = encode_bu(text)
    compressed_text = compress_lzw(encoded_text)
    print(f'Your encoded and compressed text (in binary): {encode_binary(compressed_text)}')
    print(f'Line number from B-U algorithm (in binary): {encode_binary(str(number))}')


if __name__ == '__main__':
    main()
